//
//  CustomInfoWindow.swift
//  CustomInfoWindow
//
//  Created by Malek T. on 12/13/15.
//  Copyright © 2015 Medigarage Studios LTD. All rights reserved.
//

import UIKit

class CustomInfoWindow: UIView {

    
    @IBOutlet weak var popUpImage: UIImageView!
    
    @IBOutlet weak var artTitle: UILabel!

    @IBOutlet weak var artist: UILabel!
    
    @IBOutlet weak var medium: UILabel!
    
    @IBOutlet weak var address: UILabel!

    @IBOutlet weak var info: UITextView!

    @IBOutlet weak var exit: UIButton!
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
