//
//  MakeUrlToGetImagesFromServer.swift
//  SPAR
//  This class is used to create image url
//  from the image names pulled from the database.
// Using this class and the url , image objects
// are created from the specified url.

//  Created by MCS on 3/2/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//
import UIKit
import MapKit
import Foundation
class MakeUrlToGetImagesFromServer
{
    private let artInfoParser:ArtInfoParser
    private var imageArray: [UIImage]
    
  
    
    init()
    {
        artInfoParser = ArtInfoParser()
        imageArray = [UIImage]()
    }
    
    //makes the correct url for each image names
    func addImageUrlToArray(artInfoUrl: String, imageUrl:String) -> [UIImage]
    {
        artInfoParser.parseArtInfoJson(artInfoUrl)
        
        var tempImageArray = artInfoParser.getImages
        
        for var index = 0; index < tempImageArray.count; ++index{
            let url = NSURL(string: imageUrl + tempImageArray[index])
            let data = NSData(contentsOfURL: url!)
            imageArray.append(UIImage(data: data!)!)
        }
        return imageArray
    }
}