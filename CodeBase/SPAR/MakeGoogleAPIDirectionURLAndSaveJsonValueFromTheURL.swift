//
//  MakeGoogleAPIDirectionURLAndSaveJsonValueFromTheURL.swift
//  SPAR
//
//  Created by MCS on 2/7/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation
import GoogleMaps
import SwiftyJSON

class MakeGoogleAPIDirectionURLAndSaveJsonValueFromTheURL {
    
    private let googleUrl = "https://maps.googleapis.com/maps/api/directions/json?"
    private let googleDirectionAPIKey = "&key=AIzaSyAjXYPby3Xv8GfoH1vjtplRnTY6pRqPgOs"
    
    private var parseJson:ParseJSON
    
    var startingLatArray, startingLongArray,endingLatArray,endingLongArray:[Double]
    
    private var count:Int
    
    
    init(parseJson:ParseJSON)
    {
        //parseJson = ParseJSON()
        self.parseJson = parseJson
        
        startingLatArray = [Double]()
        startingLongArray = [Double]()
        endingLatArray = [Double]()
        endingLongArray = [Double]()
        count = 0
    }
    
    
    func makeGoogleAPIUrl(startingLatitude:String, startingLongitude:String, endingLatitude:String, endingLongitude:String) -> String
    {
        return (googleUrl + "origin=" + startingLatitude + "," + startingLongitude + "&destination=" + endingLatitude + "," + endingLongitude+googleDirectionAPIKey).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    }
    
    
    func saveAllLatitudeAndLongitudeInAnArray(baseUrl:String)
    {
        var sLat = parseJson.parseJson(baseUrl).startLatArray
        var sLong = parseJson.parseJson(baseUrl).startLongArray
        var eLat = parseJson.parseJson(baseUrl).endLatArray
        var eLong = parseJson.parseJson(baseUrl).endLongArray
        
        var count = sLat.count
        
        for var index = 0; index < count; ++index{
            startingLatArray.append(sLat[index])
            startingLongArray.append(sLong[index])
            endingLatArray.append(eLat[index])
            endingLongArray.append(eLong[index])
        }
        
        sLat.removeAll()
        sLong.removeAll()
        eLat.removeAll()
        eLong.removeAll()
        
        count = 0
        
    }
    
    var getParseJsonCount: Int
        {
        get{
            return count
        }
    }
    
    
    var returnStartLatArray: [Double]
        {
        get{
            return startingLatArray
        }
    }
    
    var retrunStartLongArray: [Double]
        {
        get{
            return startingLongArray
        }
    }
    
    var returnEndLatArray: [Double]
        {
        get{
            return endingLatArray
        }
    }
    
    var returnEndLongArray: [Double]
        {
        get{
            return endingLongArray
        }
    }
    
}