//
//  MarkerLatLong.swift
//  SPAR
//  This class parses the json for the latitude 
// and longitude to be used in the directions
// api web service.

//  Created by MCS on 2/16/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation
import SwiftyJSON

class MarkerLatLong: JSONParserManager
{
    
    
    private var markerLatArray: [Double]
    
    private var markerLongArray: [Double]
    
    override init()
    {
        markerLatArray = [Double]()
        
        markerLongArray = [Double]()
    }
    
    
    /*
        This function is used to parse the latitude
        and longitude value from json.
    */
    func parseLatLongJSON(phpUrl: String)
    {
        //sanitizing the url
        var phpJson = super.connectToAPI((phpUrl).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
        
        
        
       var  phpJsonCount = phpJson.count
        
        if(phpJsonCount <= 0)
        {
            
                print("Can not parse data at this time......")
                print ("Some thing went wrong, please try to load again.....")
            
        }
        else
        {
            for var index = 0; index < phpJsonCount; ++index
            {
                
                markerLatArray.append(phpJson[index]["location_lat"].doubleValue)
                
                markerLongArray.append(phpJson[index]["location_long"].doubleValue)

            }
 
        }

     
    }
    
    //the following functions are not used in the app but would be 
    // useful if the snap to road api or directions api is used in the future.
    var returnOriginLatLong: String{
       return "\(markerLatArray[0])" + "," + "\(markerLongArray[0])"
        
    }
    
    var returndestinationLatLong: String{
        return "\(markerLatArray[markerLatArray.count - 1])" + "," + "\(markerLongArray[markerLongArray.count - 1])"
    }
    
    var returnMarkerLatArray: [Double]
        {
        get{
           
            return markerLatArray
        }
  
    }
    
    
    var convertLatLongToString: String{
        var latLongString = ""
        if((markerLatArray.count)  == (markerLongArray.count))
        {
            for var index = 1; index < markerLatArray.count - 1; ++index{
                
                latLongString += "\(markerLatArray[index])"
                latLongString += ","
                latLongString += "\(markerLongArray[index])"
                if(index != markerLatArray.count - 2)
                {
                   latLongString += "|"
                }
                
                
            }
        }
        
       
        return latLongString
    }
    
    var retrunMarkerLongArray: [Double]
        {
        get{
            return markerLongArray
        }
    }
}
