//
//  MakeDirectionAPIAndSnapToRoadAPIUrl.swift
//  SPAR
//
//  Created by MCS on 2/16/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation

class MakeDirectionAPIAndSnapToRoadAPIUrl
{
    //these four string (directionApiUrl, destination,
    //waypoints and directionApiKey) is used to make url for 
    //direction API
    private let directionApiUrl = "https://maps.googleapis.com/maps/api/directions/json?origin="
    
    private let destination = "&destination="
    
    private let wayPoints = "&waypoints=optimize:true|"
    
    let directionApiKey = "&key=AIzaSyAjXYPby3Xv8GfoH1vjtplRnTY6pRqPgOs"
    

    private let directionParser:DirectionAPIParser
    private let markerLatLongToCallStringMethod:MarkerLatLong
    
   
    private let snapToRoadUrl = "https://roads.googleapis.com/v1/snapToRoads?path="
    
    private let interpolate = "&interpolate=true&key="
    
    private let snapToRoadAPIKey = "AIzaSyA5RZkb6DAylPWTiaAYXSUDBYIDQdvs48w"
    
    init()
    {
        
        directionParser = DirectionAPIParser()
        markerLatLongToCallStringMethod = MarkerLatLong()
        //snapToRoadAPIUrl()
       
    }
    
/*
    This function makes the url for directionApi
*/
    func makeDirectionAPIUrl(url:String) -> String
    {
        var urlString = ""
        markerLatLongToCallStringMethod.parseLatLongJSON(url)
        
        urlString += directionApiUrl
        urlString += markerLatLongToCallStringMethod.returnOriginLatLong
        urlString += destination
        urlString += markerLatLongToCallStringMethod.returndestinationLatLong
        urlString += wayPoints
        urlString += markerLatLongToCallStringMethod.convertLatLongToString + directionApiKey
      print(urlString)
        return (urlString).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
    }
    
    /*
    this function is not used for sprint 2
    but will be used in sprint 3.
*/
    func snapToRoadAPIUrl() -> String
    {
        var snapToRoad = ""
        var snapToRoadString = ""
        var directionUrl = makeDirectionAPIUrl("http://mcs.drury.edu/spar/snaptoroad.php")
        
        directionParser.parseDirectionAPIJson(directionUrl)
        
        var startLArray = directionParser.sLatArray
        var startLongArray = directionParser.sLongArray
        var endLArray = directionParser.eLatArray
        var endLongArray = directionParser.eLongArray
        
      
        
        if(startLArray.count == startLongArray.count && startLongArray.count
            == endLArray.count && endLArray.count == endLongArray.count)
        {
            for var index = 0; index < startLArray.count; ++index{
                snapToRoadString += "\(startLArray[index])"
                snapToRoadString += ","
                snapToRoadString += "\(startLongArray[index])"
                snapToRoadString += "|"
                
                
                
                snapToRoadString += "\(endLArray[index])"
                snapToRoadString += ","
                snapToRoadString += "\(endLongArray[index])"
                if(index != endLArray.count - 1)
                {
                    snapToRoadString += "|"
                }
                
            }
            
            snapToRoad += snapToRoadUrl
            snapToRoad += snapToRoadString
            snapToRoad += interpolate
            snapToRoad += snapToRoadAPIKey
        }
        
        
        return (snapToRoad).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    }
    
    

        
        

}

