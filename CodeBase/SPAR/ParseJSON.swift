//
//  ParseJSON.swift
//  SPAR
//
//  Created by Anjan Shrestha on 12/1/15.
//  Copyright © 2015 Z.A.C. Overflow. All rights reserved.
//

import Foundation
import SwiftyJSON

//AIzaSyBmmuPcf8U3hPN4CpLQCgXmO_zMKbwyq8o
class ParseJSON
{
    //private let URL = "https://maps.googleapis.com/maps/api/directions/json?"
    //private let APIKEY = "AIzaSyAjXYPby3Xv8GfoH1vjtplRnTY6pRqPgOs"
    
    
    
    var startLatitudeArray:[Double]
    var startLongitudeArray:[Double]
    var endLatitudeArray:[Double]
    var endLongitudeArray:[Double]
    var count:Int
    
    init()
    {
       startLatitudeArray = [Double]()
       startLongitudeArray = [Double]()
       endLatitudeArray = [Double]()
       endLongitudeArray = [Double]()
        count = 0
        
    }
    
/*
    This function reads a json file and parses the latitude and longitude from it..
    
*/
    
    func parseJson(baseUrl:String) -> (startLatArray: [Double], startLongArray:[Double], endLatArray: [Double], endLongArray: [Double] ){
        
        let url = NSURL(string: baseUrl)
        //print(url)
       
        let data = NSData(contentsOfURL: url!)as NSData!
        let readableJSON = JSON(data: data, options: NSJSONReadingOptions.MutableContainers, error: nil)
        
        
        //count = readableJSON["routes"][0]["legs"][0]["steps"].count
        count = readableJSON["routes"][0]["legs"].count
        //print(readableJSON["routes"][0]["legs"][2]["steps"][4]["start_location"])
        
        var stepCount = 0
        for var index1 = 0; index1 < count; ++index1{
            var index2 = 0
            while (index2 < (readableJSON["routes"][0]["legs"][index1]["steps"].count))
            {
                ++stepCount
                startLatitudeArray.append(readableJSON["routes"][0]["legs"][index1]["steps"][index2]["start_location"]["lat"].doubleValue)
             
                startLongitudeArray.append(readableJSON["routes"][0]["legs"][index1]["steps"][index2]["start_location"]["lng"].doubleValue)
                
                endLatitudeArray.append(readableJSON["routes"][0]["legs"][index1]["steps"][index2]["end_location"]["lat"].doubleValue)
                
                endLongitudeArray.append(readableJSON["routes"][0]["legs"][index1]["steps"][index2]["end_location"]["lng"].doubleValue)
                //print(endLongitudeArray[index2])
                
                index2++
                
            }
            
        }
        print(stepCount)
        //print(count)
        
       
        
        
        /*
        for var index = 0; index < count; ++index {
            
            startLatitudeArray.append(readableJSON["routes"][0]["legs"][index]["steps"][0]["start_location"]["lat"].doubleValue)
           //print(startLatitudeArray[index])
            
            startLongitudeArray.append(readableJSON["routes"][0]["legs"][index]["steps"][0]["start_location"]["lng"].doubleValue)
           // print(startLongitudeArray[index])
            
            endLatitudeArray.append(readableJSON["routes"][0]["legs"][index]["steps"][0]["end_location"]["lat"].doubleValue)
            //print(endLatitudeArray[index])
            
            endLongitudeArray.append(readableJSON["routes"][0]["legs"][index]["steps"][0]["end_location"]["lng"].doubleValue)
            //print(endLongitudeArray[index])
            
            //print(startLongitudeArray[index])
        }
        
        */
        return (startLatitudeArray, startLongitudeArray, endLatitudeArray, endLongitudeArray)
    }
    
 
    var stepCount:Int
        {
        get{
            return count
        }
    }

  /*
    
/*
    This method is the getter and setter for the originLatitude
*/

    var startingLat:String
        {
            get{
                  return originLatitude;
               }
        
            set{
                  self.originLatitude = newValue
                }
    }
    
    
    /*
    This method is the getter and setter for the originLongitude
    
    
    */
    
    var startingLong:String
    {
        get{
            return originLongitude
        }
        set{
            self.originLongitude = newValue
        }
        
    }
    
    
    /*
    This method is the getter and setter for the destinationLatitude
    
    
    */
    var endingLat:String
    {
        get{
            return destinationLatitude
        }
        set{
            self.destinationLatitude = newValue
        }
        
    }
    
    /*
    This method is the getter and setter for the destinationLongitude
    
    
    */
    var endingLong:String
    {
        get{
            return destinationLongitude
        }
        set{
            self.destinationLongitude = newValue
        }
        
    }
   */
    
}