//
//  SnapToRoadJSONParser.swift
//  SPAR
//
//  Created by MCS on 2/17/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation
import SwiftyJSON

class SnapToRoadParser:JSONParserManager
{
    private var longitudeArray: [Double]
    
    private var latitudeArray: [Double]
    
    private let urlMaker:MakeDirectionAPIAndSnapToRoadAPIUrl
    
    private var count:Int
    
    override init() {
        
        urlMaker = MakeDirectionAPIAndSnapToRoadAPIUrl()
        
        longitudeArray = [Double]()
        latitudeArray = [Double]()
        
        count = 0
    }
    
    
    func snapToRoadJsonParser(baseUrl: String)
    {
        var snapToRoadJson =  super.connectToAPI(baseUrl)
        
        
        count = snapToRoadJson["snappedPoints"].count
        
        
        
        
        for var index = 0; index < count; ++index{
            latitudeArray.append(snapToRoadJson["snappedPoints"][index]["location"]["latitude"].doubleValue)
            
            longitudeArray.append(snapToRoadJson["snappedPoints"][index]["location"]["longitude"].doubleValue)
           
        }
        
    }
    
    var latArray: [Double]
        {
        get{
            return latitudeArray
        }
    }
    
    var longArray: [Double]
        {
        get{
            return longitudeArray
        }
    }
    
}
