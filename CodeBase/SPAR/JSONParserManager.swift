//
//  JSONParserManager.swift
//  SPAR
//  This class is used as a parent class 
// for all the classes that uses web services and
// needs to connect to the internet.
//
//  Created by MCS on 2/16/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation
import SwiftyJSON

class JSONParserManager
{
    init () { }
    
    /*
        This function is used to connect
        to the specifeid url and JSON object is 
        returned.
    */
    func connectToAPI(baseUrl: String) -> JSON
    {
        let url = NSURL(string: baseUrl)
        
        let data = NSData(contentsOfURL: url!)as NSData!
        let readableJSON = JSON(data: data, options: NSJSONReadingOptions.MutableContainers, error: nil)
        
        return readableJSON
    }
}
