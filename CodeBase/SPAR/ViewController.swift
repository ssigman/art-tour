//
//  ViewController.swift
//  SPAR
// This is the class from where the app 
// is loaded to be used by the user.

//  Created by Shrestha on 12/1/15.
//  Copyright © 2015 Z.A.C. Overflow. All rights reserved.
//

import UIKit
import MapKit
import SwiftyJSON
import GoogleMaps


class ViewController: UIViewController, GMSMapViewDelegate, UIGestureRecognizerDelegate {
    
    //the Google Map view
    var mapView:GMSMapView!
    
    //constant representing the screen size of the device on which SPAR is launched
    let screenSize: CGSize = UIScreen.mainScreen().bounds.size
    
    //the width of the waypoint pop-ups
    let RegularWindowWidth: CGFloat = 276
    let IpadWindowWidth: CGFloat = 500
    let SmallWindowWidth: CGFloat = 276
    
    //the height of the waypoint pop-ups
    let RegularWindowHeight: CGFloat = 455
    let IpadWindowHeight: CGFloat = 800
    let SmallWindowHeight: CGFloat = 360
    
    //the height of the info bar at the top of the SPAR application
    let infoBarHeight: CGFloat = 64
    
    //url to get the lat and long from the web server
    let phpLatLongUrl = "http://mcs.drury.edu/spar/connection.php"
    
    // part of url to create image object from given url
    let imageInfoUrl = "http://mcs.drury.edu/spar/sparimage/"
    
    //art info url
    let artInfoParserUrl = "http://mcs.drury.edu/spar/artinfo.php"
    
    //artwork database info url
    let artworkDatabaseInfoUrl = "http://mcs.drury.edu/spar/serverQueries.php"
    
    // url to pull the data necessary to draw polyline
    let url = "http://mcs.drury.edu/spar/snaptoroad.php"
    
    //artInfoRetriever object to pull info from database
    var artInfoRetriever:ArtInfoRetriever = ArtInfoRetriever()
    
    //num of subviews sans pop-ups
    let numSubViewsSansPopups = 2
    
    //num of markers in the app
    let numOfMarkers = 13
    
    //instansiating marker object
    let marker = Marker()
    
    //used to parse lat and long
    let markerLatLong = MarkerLatLong()
    
    //used to draw the marker on the map
    let markerOnTheMap = DrawMarkerOnTheMap()
    
    //class to convert image into image object
    let makeImageInfoUrl = MakeUrlToGetImagesFromServer()
    
    //arrays to store parsed json values from goolge api
    var startLatitudeArray = [Double]()
    var startLongitudeArray = [Double]()
    var endLatitudeArray = [Double]()
    var endLongitudeArray = [Double]()
    
    //is used to set up initial map view when the app is loaded.
    let springfieldLatitude = 37.21
    let springfieldLongitude = -93.2861
    
    //initial zoom level of map depending on different screen sizes.
    let initZoomLevelIpad:Float = 14.0
    let initZoomLevelIphoneRegular:Float = 13.0
    let initZoomLevelIphoneSmall:Float = 12.8
    
    // title of the artwork
    var titleArray = [String]()
    
    // name of the artist
    var artistArray = [String]()
    
    //artwork medium
    var mediumArray = [String]()
    
    //information about the artwork
    var infoArray = [String]()
    
    //address of the artwork
    var addressArray = [String]()
    
    //image array
    var stringArray = [UIImage]()
    
    // array for markers
    var markerArray = [GMSMarker]()
    
    /*
    This function loads the application and plots waypoints and routes.
    */
    
    override func viewDidLoad() {
     
     super.viewDidLoad()
        
      //checks if the device is connected to the internet.
      if Connection.isConnectedToNetwork(){
            
        artInfoRetriever = ArtInfoRetriever(baseURL: "http://mcs.drury.edu/spar/serverQueries.php")
        
        var camera:GMSCameraPosition
        
        //setup map view to springfield, MO
        if(screenSize.height > 736) {
            camera = GMSCameraPosition.cameraWithLatitude(springfieldLatitude, longitude: springfieldLongitude, zoom: initZoomLevelIpad)
        }
        else if(screenSize.height > 480) {
            camera = GMSCameraPosition.cameraWithLatitude(springfieldLatitude, longitude: springfieldLongitude, zoom: initZoomLevelIphoneRegular)
        }
        else {
            camera = GMSCameraPosition.cameraWithLatitude(springfieldLatitude, longitude: springfieldLongitude, zoom: initZoomLevelIphoneSmall)
        }
        
        // calls necessary functions to parse art info from the JSON
        titleArray = artInfoRetriever.parseTitleFromJSON()
        artistArray = artInfoRetriever.parseArtistFromJSON()
        mediumArray = artInfoRetriever.parseMediumFromJSON()
        infoArray = artInfoRetriever.parseDescriptionFromJSON()
        addressArray = artInfoRetriever.parseAddressFromJSON()
        
        mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        
        self.mapView.delegate = self
        
        //adds image url to the array
        stringArray = makeImageInfoUrl.addImageUrlToArray(artInfoParserUrl, imageUrl: imageInfoUrl)
        
        
        
        markerLatLong.parseLatLongJSON(phpLatLongUrl)
        
        
        //latitude of markers
        var markerLatArray = (markerLatLong.returnMarkerLatArray)
        
        //longitude of markers
        var markerLongArray = (markerLatLong.retrunMarkerLongArray)
        
        //adds specified number of markers in the array
        markerArray = marker.numOfMarkersInArray(numOfMarkers)
        
        // this for loop is primarily used to draw the marker on the map
        // on the specified latitude and longitude.
        // Also accessibilityLabel is used to access the index of marker
        // when it is clicked to display the correct info about the 
        // marker(art installtion) tapped.
        for var index = 0; index < markerArray.count; ++index{
            markerOnTheMap.drawMarker(markerArray[index], lat: markerLatArray[index], long: markerLongArray[index], snippet: "")
            markerArray[index].infoWindowAnchor = CGPointMake(0.0, -0.2)
            
            markerArray[index].accessibilityLabel = "\(index)"
            markerArray[index].map = self.mapView
            
        }
        
        
        
        let markerLatLang1 = MarkerLatLong()
        markerLatLang1.parseLatLongJSON(url)
        
        startLatitudeArray = markerLatLang1.returnMarkerLatArray
        startLongitudeArray = markerLatLang1.retrunMarkerLongArray
        
        //the path for the polyline
        let path = GMSMutablePath()
        
        //loops through the latitude and longitude arrays, and adds coordinates to the path.startLatitudeArray.count
        for var index = 0; index < startLatitudeArray.count; ++index{
            path.addCoordinate(CLLocationCoordinate2D(latitude: startLatitudeArray[index] , longitude:  startLongitudeArray[index]))
            
            
        }
        
        //the polyline used to display a route.
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.geodesic = true
        polyline.map = mapView
        
        
        self.view = mapView
        self.mapView.myLocationEnabled = true
        
        self.mapView.settings.myLocationButton = true;
        self.mapView.settings.compassButton = true;
        
        if(screenSize.height > 736) {
            mapView.setMinZoom(14, maxZoom: 17)
        }
        else if(screenSize.height > 480) {
            mapView.setMinZoom(13, maxZoom: 17)
        }
        else {
            mapView.setMinZoom(12.5, maxZoom: 17)
        }
        
        }
        // user is alerted to check the internet connection 
        // if the device is not connected to the internet.
        else{
            let alertController = UIAlertController(title: "No Internet Connection", message: "Please make sure the device is connected to the internet. Close the app, then restart.", preferredStyle: .Alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alertController.addAction(defaultAction)
            
            presentViewController(alertController, animated: true, completion: nil)
        }
    
    
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func removePopup(sender: UIButton) {
        self.mapView.subviews[mapView.subviews.count - 1].removeFromSuperview()
        self.mapView.settings.scrollGestures = true
        self.mapView.settings.zoomGestures = true
        self.mapView.settings.myLocationButton = true
    }
    
    //MARK: GMSMapViewDelegate
    func mapView(mapView: GMSMapView!, markerInfoWindow marker: GMSMarker!) -> UIView! {
        
        var infoWindow:CustomInfoWindow
        
        //IPAD
        if (screenSize.height > 736){
            infoWindow = NSBundle.mainBundle().loadNibNamed("IpadWindow", owner: self, options: nil).first! as! CustomInfoWindow
            infoWindow.frame = CGRectMake((screenSize.width / 2) - (IpadWindowWidth / 2),((screenSize.height + infoBarHeight) / 2) - (IpadWindowHeight / 2), IpadWindowWidth, IpadWindowHeight)
        }
        //REGULAR - iPhone 5(s), 6(s), 6 plus
        else if (screenSize.height > 480){
            infoWindow = NSBundle.mainBundle().loadNibNamed("RegularWindow", owner: self, options: nil).first! as! CustomInfoWindow
            infoWindow.frame = CGRectMake((screenSize.width / 2) - (RegularWindowWidth / 2),((screenSize.height + infoBarHeight) / 2) - (RegularWindowHeight / 2), RegularWindowWidth, RegularWindowHeight)
        }
        //SMALL - iPhone 4(s)
        else {
            infoWindow = NSBundle.mainBundle().loadNibNamed("SmallWindow", owner: self, options: nil).first! as! CustomInfoWindow
            infoWindow.frame = CGRectMake((screenSize.width / 2) - (SmallWindowWidth / 2),((screenSize.height + infoBarHeight) / 2) - (SmallWindowHeight / 2), SmallWindowWidth, SmallWindowHeight)
        }
        
        //Dummy UIView to allow return type on what ought to be a void method
        let hiddenUIView = NSBundle.mainBundle().loadNibNamed("RegularWindow", owner: self, options: nil).first! as! CustomInfoWindow
        
        
        let index:Int! = Int(marker.accessibilityLabel!)
        
        infoWindow.popUpImage.image = stringArray[index]
        infoWindow.artTitle.text = titleArray[index]
        infoWindow.artist.text = artistArray[index]
        infoWindow.medium.text = mediumArray[index]
        infoWindow.info.text = infoArray[index]
        
        infoWindow.info.editable = false
        self.mapView.settings.myLocationButton = false
        infoWindow.address.text = addressArray[index]
        infoWindow.exit.addTarget(self, action: "removePopup:", forControlEvents: UIControlEvents.TouchUpInside)
        
        hiddenUIView.hidden = true
        
        if(self.mapView.subviews.count == numSubViewsSansPopups) {
            self.mapView.addSubview(infoWindow)
            markerArray[index].icon = GMSMarker.markerImageWithColor(UIColor.blueColor())
            self.mapView.settings.scrollGestures = false
            self.mapView.settings.zoomGestures = false
        }

        hiddenUIView.frame = CGRectMake(0, 0, 0, 0)
        
        
        return hiddenUIView
    }
    
}

