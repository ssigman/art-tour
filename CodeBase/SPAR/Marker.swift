//
//  Marker.swift
//  SPAR
//  This class is used to create an 
//  array of markers to be used in the app

//  Created by MCS on 2/16/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation
import GoogleMaps

class Marker
{
    private var markerArray: [GMSMarker]
    

    
    init()
    {
        markerArray = [GMSMarker]()
        
    }
   /*
    This function adds the specified number of markers in the array
    @return - an array containing markers as the data
    */
    func numOfMarkersInArray(num: Int) -> [GMSMarker]
    {
        for var index = 0; index < num; ++index{
            markerArray.append(GMSMarker())
            
           
        }
         return markerArray
    }
    
}