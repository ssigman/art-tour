//
//  DirectionAPIParser.swift
//  SPAR
//
//  Created by MCS on 2/16/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation
import SwiftyJSON

class DirectionAPIParser:JSONParserManager
{
    
    private var startLatArray: [Double]
    
    private var startLongArray: [Double]
    
    private var endLatArray: [Double]
    
    private var endLongArray: [Double]
    
    private var count: Int
    
    override init()
    {
        startLatArray = [Double] ()
        
        startLongArray = [Double]()
        
        endLatArray = [Double]()
        
        endLongArray = [Double]()
        
        count = 0
        
      
        
    }
    
    //This function parses the json data returned from 
    //google directions api
    func parseDirectionAPIJson(baseUrl: String)
    {
        var directionApiJson = super.connectToAPI(baseUrl)
        
        // total number of legs count
        count = directionApiJson["routes"][0]["legs"].count
       
        
        //var stepCount = 0
        
        if(directionApiJson["status"] != "OK")
        {
            print("Error.... while connecting to Directions API")
        }
        else{
        for var index1 = 0; index1 < count; ++index1{
            var index2 = 0
            while (index2 < (directionApiJson["routes"][0]["legs"][index1]["steps"].count))
            {
                //++stepCount
                startLatArray.append(directionApiJson["routes"][0]["legs"][index1]["steps"][index2]["start_location"]["lat"].doubleValue)
                
                startLongArray.append(directionApiJson["routes"][0]["legs"][index1]["steps"][index2]["start_location"]["lng"].doubleValue)
                
                endLatArray.append(directionApiJson["routes"][0]["legs"][index1]["steps"][index2]["end_location"]["lat"].doubleValue)
                
                endLongArray.append(directionApiJson["routes"][0]["legs"][index1]["steps"][index2]["end_location"]["lng"].doubleValue)
               
                
                index2++
                
            }
            
        }
        }
       
       
    }
    
    //this function is not being used at this time.
    //may be useful later.
    func convertDirectionAPILatLongToString(url: String) -> String{
        var startLatLongString = ""
        var endLatLongString = ""
        var finalString = ""
        parseDirectionAPIJson(url)
        var a = sLatArray
        var b = sLongArray
        var c = eLatArray
        var d = eLongArray
        
        for var index = 0; index < a.count; ++index{
            startLatLongString += "\(a[index])"
            startLatLongString += ","
            endLatLongString += "\(b[index])"
            startLatLongString += "|"
            endLatLongString += "\(c[index])"
            endLatLongString += "\(d[index])"
            if(index != a.count - 1)
            {
                endLatLongString += "|"
            }
            
            
        }
        finalString += startLatLongString + endLatLongString

        return finalString
        
           }
    
    var sLatArray: [Double]
        {
        get{
            return startLatArray
        }
    }
    
    var sLongArray: [Double]
        {
        get{
            return startLongArray
        }
    }
    
    var eLatArray: [Double]
        {
        get{
            return endLatArray
        }
    }
    
    var eLongArray: [Double]
        {
        get{
            return endLongArray
        }
    }
    
    
    
    
}


