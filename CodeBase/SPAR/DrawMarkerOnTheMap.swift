//
//  DrawMarkerOnTheMap.swift
//  SPAR

//  This class is used to draw markers on the map.

//  Created by MCS on 2/16/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation
import GoogleMaps

class DrawMarkerOnTheMap
{
    
    
    init()
    {
      
    }
    
    /*
    This function is used to draw markers on the map
    */
    func drawMarker(marker: GMSMarker, lat:Double, long:Double, snippet:String)
    {
    
                marker.position = CLLocationCoordinate2DMake(lat, long)
                marker.snippet = snippet

    }
}
