//
//  ArtInfoParser.swift
//  SPAR
//This class is used to parse the art information from
// the database. At this moment we are just parsing 
// the image names from the database. Rest will be 
// parsed and used in the third sprint.......
//
//  Created by MCS on 3/2/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation

class ArtInfoParser:JSONParserManager
{
    private var pictArray: [String]
    
    override init()
    {
        pictArray = [String]()
    }
    
    
    //parses images name from the database
    func parseArtInfoJson(baseUrl:String)
    {
        var artInfoJson = super.connectToAPI(baseUrl)
        
        let count = artInfoJson.count
        print(count)
        
        if(count <= 0)
        {
            print("can not parse images at this point")
            print("check your internet connection")
        }
        else{
            for var index = 0; index < count; ++index{
                pictArray.append(artInfoJson[index]["artwork_image"].stringValue)
            }
        }
    }
    
    //returns pictArray
    var getImages:[String]
        {
        return pictArray
    }
}