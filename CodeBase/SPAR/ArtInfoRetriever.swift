//
//  artInfoRetriever.swift
//  SPAR
//
//  Created by Anjan Shrestha on 3/31/16.
//  Copyright © 2016 Z.A.C. Overflow. All rights reserved.
//

import Foundation
import SwiftyJSON

class ArtInfoRetriever:JSONParserManager {
    
    let titleIndex = 0
    let mediumIndex = 1
    let descriptionIndex = 2
    let artistFNameIndex = 3
    let artistLNameIndex = 4
    let artworkToArtistIndex = 5
    let addressIndex = 6
    var artInfoJSON:JSON
    
    override init() {
        artInfoJSON = JSON("")
    }
    
    convenience init(baseURL:String) {
        self.init()
        artInfoJSON = connectToAPI((baseURL).stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
    }
    
    func parseTitleFromJSON() -> [String]{
        var titleArray:[String] = [String]()
        
        for(var i = 0; i < artInfoJSON[titleIndex].count; i++) {
            let title = String(artInfoJSON[titleIndex][i]["artwork_title"])
            titleArray.append(title)
        }
        return titleArray
    }
    
    func parseMediumFromJSON() -> [String] {
        var mediumArray:[String] = [String]()
        
        for(var i = 0; i < artInfoJSON[mediumIndex].count; i++) {
            let medium = String(artInfoJSON[mediumIndex][i]["artwork_medium"])
            mediumArray.append(medium)
        }
        return mediumArray
    }
    
    func parseDescriptionFromJSON() -> [String]{
        var descriptionArray:[String] = [String]()
        
        for(var i = 0; i < artInfoJSON[descriptionIndex].count; i++) {
            let description = String(artInfoJSON[descriptionIndex][i]["artwork_description"])
            descriptionArray.append(description)
        }
        return descriptionArray
    }
    
    func parseArtistFromJSON() -> [String]{
        var artistArray:[[String]] = [[String]]()
        var result:[String] = [String]()
        
        for(var i = 0; i < artInfoJSON[artworkToArtistIndex].count; i++) {
            var tempArtistArray:[String] = [String]()
            let artistFName = String(artInfoJSON[artworkToArtistIndex][i]["artist_fname"])
            let artistLName = String(artInfoJSON[artworkToArtistIndex][i]["artist_lname"])
            
            if(artistFName != "null" && artistLName != "null") {
                tempArtistArray.append(artistFName + " " + artistLName)
            } else if(artistFName == "null" && artistLName != "null") {
                tempArtistArray.append(artistLName)
            } else if(artistFName != "null" && artistLName != "null") {
                tempArtistArray.append(artistFName)
            }
            
            for(var j = i + 1; j < artInfoJSON[artworkToArtistIndex].count; j++) {
                let artistFName = String(artInfoJSON[artworkToArtistIndex][j]["artist_fname"])
                let artistLName = String(artInfoJSON[artworkToArtistIndex][j]["artist_lname"])
                if(String(artInfoJSON[artworkToArtistIndex][j]["artwork_title"]) == String(artInfoJSON[artworkToArtistIndex][i]["artwork_title"])) {
                    if(artistFName != "null" && artistLName != "null") {
                        tempArtistArray.append(artistFName + " " + artistLName)
                        i++
                    } else if(artistFName == "null" && artistLName != "null") {
                        tempArtistArray.append(artistLName)
                        i++
                    } else if(artistFName != "null" && artistLName == "null") {
                        tempArtistArray.append(artistFName)
                        i++
                    }
                }
            }
            
            if(!tempArtistArray.isEmpty) {
                artistArray.append(tempArtistArray)
            }
        }
        
        for(var i = 0; i < artistArray.count; i++) {
            var artistString = String()
            for(var j = 0; j < artistArray[i].count; j++) {
                if(j > 0) {
                    artistString = artistString + ", "
                }
                artistString = artistString + artistArray[i][j]
            }
            result.append(artistString)
        }
        
        return result
    }
    
    func parseAddressFromJSON() -> [String] {
        var addressArray:[String] = [String]()
        
        for(var i = 0; i < artInfoJSON[addressIndex].count; i++) {
            let locationStreetNumber = String(artInfoJSON[addressIndex][i]["location_street_number"]) + " "
            let locationStreetName = String(artInfoJSON[addressIndex][i]["location_street_name"]) + ", "
            let locationCity = String(artInfoJSON[addressIndex][i]["location_city"]) + " "
            let locationState = String(artInfoJSON[addressIndex][i]["location_state"]) + " "
            let locationZip = String(artInfoJSON[addressIndex][i]["location_zip"])
            addressArray.append(locationStreetNumber + locationStreetName + locationCity + locationState + locationZip)
        }
        
        return addressArray
    }
}
