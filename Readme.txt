Springfield Public Art Route (SPAR)

Description:
The SPAR iOS application will allow art enthusiasts 
to tour venues from Commercial Street all the way to 
Downtown Springfield.The app will outline a suggested path
for a tour and the user will be able to view information on
venues and art within those venues as they progress on their
tour. 


CSCI 371 - Software Engineering

Team Members:
Zach Crownover - Project Manager
Anjan Shrestha - Team Manager
Cameron Falk- Technical Leader